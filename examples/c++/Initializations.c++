// -------------------
// Initializations.c++
// -------------------

// http://en.cppreference.com/w/cpp/language/value_initialization
// http://en.cppreference.com/w/cpp/language/initializer_list
// http://en.cppreference.com/w/cpp/utility/initializer_list

#include <cassert>          // assert
#include <initializer_list> // initializer_list
#include <iostream>         // cout, endl
#include <vector>           // vector

using namespace std;

void test1 () {
    int i(2);
    assert(i == 2);

    int j = 2;
    assert(j == 2);

    j = 3;
    assert(j == 3);

    int k{2};
    assert(k == 2);

    int m = {2};
    assert(m == 2);

    m = {3};
    assert(m == 3);}

void test2 () {
    int i(2.0);      // narrowing
    assert(i == 2);

    int j = 2.0;
    assert(j == 2);

	j = 2.0;

//  int k{2.0};      // error: type 'double' cannot be narrowed to 'int' in initializer list [-Wc++11-narrowing]

//  int k = {2.0};   // error: type 'double' cannot be narrowed to 'int' in initializer list [-Wc++11-narrowing]

//  k = {2.0};       // error: type 'double' cannot be narrowed to 'int' in initializer list [-Wc++11-narrowing]
    }

struct A {
    A (int, int)
        {}};

void test3 () {
    A x(2, 3);
    assert(&x);

    A z{2, 3};
    assert(&z);

    A t = {2, 3};
    assert(&t);

    t = {2, 3};
    assert(&t);}

struct B {
    explicit B (int, int)
        {}};

void test4 () {
    B x(2, 3);
    assert(&x);

    B z{2, 3};
    assert(&z);

//  B t = {2, 3}; // error: chosen constructor is explicit in copy-initialization

//  t = {2, 3};   // error: no viable overloaded '='
    }

struct C {
    C (initializer_list<int>)
        {}};

void test5 () {
//  C x(2);           // error: no matching constructor for initialization of 'C'

//  C y = 2;          // error: no viable conversion from 'int' to 'C'

//  y = 2;            // error: no match for 'operator=' (operand types are 'C' and 'int')

    C z{2};
    assert(&z);

    C t = {2};
    assert(&t);

    t = {2};
    assert(&t);}

struct D {
    explicit D (initializer_list<int>)
        {}};

void test6 () {
//  D x(2);           // error: no matching constructor for initialization of 'D'

//  D y = 2;          // error: no viable conversion from 'int' to 'D'

//  y = 2;            // error: no match for 'operator=' (operand types are 'D' and 'int')

    D z{2};
    assert(&z);

//  D t = {2};        // error: converting to 'D' from initializer list would use explicit constructor 'D::D(std::initializer_list<int>)'

//  t = {2};          // error: converting to 'D' from initializer list would use explicit constructor 'D::D(std::initializer_list<int>)'
    }

struct E {
    E (int)
        {}

    E (initializer_list<int>)
        {}

    bool operator == (const E&) {
    	return true;}};

void test7 () {
    E x(2);
    assert(&x);

    E y = 2;
    assert(&y);

    y = 2;

    E z{2};
    assert(&z);

    E t = {2};
    assert(&t);

    t = {2};
    assert(&t);

    assert(x == y);
//  assert(x == {2}); // error: expected primary-expression before '{' token
    }

void test8 () {
    vector<int> x(2);
    assert(x.size() == 2);
    assert(equal(begin(x), end(x), begin({0, 0})));

//  vector<int> y = 2;                              // error: no viable conversion from 'int' to 'vector<int>'

//  y = 2;                                          // error: no match for 'operator=' (operand types are 'std::vector<int>' and 'int')

    vector<int> z{2};
    assert(z.size() == 1);
    assert(equal(begin(z), end(z), begin({2})));

    vector<int> t = {2};
    assert(t.size() == 1);
    assert(equal(begin(t), end(t), begin({2})));

    t = {2};
    assert(t.size() == 1);
    assert(equal(begin(t), end(t), begin({2})));

    assert(z == t);
//  assert(z == {2});                               // error: expected primary-expression before '{' token
    }

int main () {
    cout << "Initializations.c++" << endl;
    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    test7();
    test8();
    cout << "Done." << endl;
    return 0;}
