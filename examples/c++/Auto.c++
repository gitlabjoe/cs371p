// --------
// Auto.c++
// --------

// http://en.cppreference.com/w/cpp/language/auto

#include <cassert>  // assert
#include <iostream> // cout, endl

using namespace std;

void test1 () {
    int i;            // uninitialized
    assert(&i == &i);

//  auto j;           // error: declaration of variable 'j' with type 'auto' requires an initializer
    }

void test2 () {
    int i = 2;
    assert(i         == 2);
    assert(sizeof(i) == sizeof(int));

    auto j = 2;
    assert(j         == 2);
    assert(sizeof(j) == sizeof(int));}

void test3 () {
    int i = 2.0;
    assert(i         == 2);
    assert(sizeof(i) == sizeof(int));

    auto j = 2.0;
    assert(j         == 2);
    assert(sizeof(j) == sizeof(double));}

void test4 () {
    int i = {2};
    assert(i         == 2);
    assert(sizeof(i) == sizeof(int));

    auto a = {2};
    assert(equal(begin(a), end(a), begin({2})));
    assert(sizeof(a) == sizeof(initializer_list<int>));}

int main () {
    cout << "Auto.c++" << endl;
    test1();
    test2();
    test3();
    test4();
    cout << "Done." << endl;
    return 0;}
