.DEFAULT_GOAL := all

all:

clean:
	cd examples/c++; make clean

config:
	git config -l

docker:
	docker run -it -v $(PWD):/usr/cs371p -w /usr/cs371p gpdowning/gcc

init:
	touch README
	git init
	git remote add origin git@gitlab.com:gpdowning/cs371p.git
	git add README
	git commit -m 'first commit'
	git push -u origin master

pull:
	make clean
	@echo
	git pull
	git status

push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add examples
	git add makefile
	git add notes
	git commit -m "another commit"
	git push
	git status

run:
	cd examples/c++; make run

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

sync:
	@rsync -r -t -u -v --delete            \
    --include "Docker.txt"                 \
    --include "Dockerfile"                 \
    --include "Hello.c++"                  \
    --include "Assertions.c++"             \
    --include "UnitTests1.c++"             \
    --include "UnitTests2.c++"             \
    --include "UnitTests3.c++"             \
    --include "Coverage1.c++"              \
    --include "Coverage2.c++"              \
    --include "Coverage3.c++"              \
    --include "IsPrime.c++"                \
    --include "IsPrimeT.c++"               \
    --include "Exceptions.c++"             \
    --include "Variables.c++"              \
    --include "Incr.c++"                   \
    --include "IncrT.c++"                  \
    --include "Arguments.c++"              \
    --include "Consts.c++"                 \
    --include "Arrays1.c++"                \
    --include "Equal.c++"                  \
    --include "EqualT.c++"                 \
    --include "Fill.c++"                   \
    --include "FillT.c++"                  \
    --include "Copy.c++"                   \
    --include "CopyT.c++"                  \
    --include "Iterators.c++"              \
    --include "FactorialT.c++"             \
    --include "RangeIterator.c++"          \
    --include "RangeIteratorT.c++"         \
    --include "Accumulate.c++"             \
    --include "AccumulateT.c++"            \
    --include "Range.c++"                  \
    --include "RangeT.c++"                 \
    --include "Auto.c++"                   \
    --include "Initializations.c++"        \
    --include "Iteration.c++"              \
    --include "Functions.c++"              \
    --include "Arrays2.c++"                \
    --include "InitializerList.c++"        \
    --include "Vector1.c++"                \
    --include "Vector1T.c++"               \
    --include "Vector2.c++"                \
    --include "Vector2T.c++"               \
    --include "FunctionOverloading.c++"    \
    --include "Move.c++"                   \
    --include "Vector3.c++"                \
    --include "Vector3T.c++"               \
    --include "Vector4.c++"                \
    --include "Vector4T.c++"               \
    --include "Shapes1.c++"                \
    --include "Shapes1T.c++"               \
    --include "MethodOverriding1.c++"      \
    --include "Shapes2.c++"                \
    --include "Shapes2T.c++"               \
    --include "MethodOverriding2.c++"      \
    --include "Shapes3.c++"                \
    --include "Shapes3T.c++"               \
    --include "Handle1.c++"                \
    --include "Handle1T.c++"               \
    --include "Handle2.c++"                \
    --include "Handle2T.c++"               \
    --include "Handle3.c++"                \
    --include "Handle3T.c++"               \
    --exclude "*"                          \
    ../../examples/c++/ examples/c++/
	@rsync -r -t -u -v --delete            \
    --include "Darwin.uml"                 \
    --include "Darwin.png"                 \
    --include "Stack1.uml"                 \
    --include "Stack1.png"                 \
    --include "Stack2.uml"                 \
    --include "Stack2.png"                 \
    --include "LifeCC.uml"                 \
    --include "LifeCC.png"                 \
    --include "LifeFC.uml"                 \
    --include "LifeFC.png"                 \
    --include "LifeC.uml"                  \
    --include "LifeC.png"                  \
    --include "Shapes1.uml"                \
    --include "Shapes1.png"                \
    --include "Shapes2.uml"                \
    --include "Shapes2.png"                \
    --include "Shapes3.uml"                \
    --include "Shapes3.png"                \
    --include "Handle1.uml"                \
    --include "Handle1.png"                \
    --include "Handle2.uml"                \
    --include "Handle2.png"                \
    --include "Handle3.uml"                \
    --include "Handle3.png"                \
    --exclude "*"                          \
    ../../examples/uml/ examples/uml/
	@rsync -r -t -u -v --delete            \
    --include "Collatz.c++"                \
    --include "Collatz.h"                  \
    --include "RunCollatz.c++"             \
    --include "RunCollatz.in"              \
    --include "RunCollatz.out"             \
    --include "TestCollatz.c++"            \
    --exclude "*"                          \
    ../../projects/c++/collatz/ projects/collatz/
	@rsync -r -t -u -v --delete            \
    --include "Allocator.h"                \
    --include "RunAllocator.c++"           \
    --include "RunAllocator.in"            \
    --include "RunAllocator.out"           \
    --include "TestAllocator.c++"          \
    --exclude "*"                          \
    ../../projects/c++/allocator/ projects/allocator

versions:
	cd examples/c++; make versions
