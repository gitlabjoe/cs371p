// -----------
// Fri, 19 Oct
// -----------

class Mammal {}

class Tiger extends Mammal {}

class T {
    public static void main (...) {
        Tiger x = new Mammal();     // no
        Mammal y = new Tiger();

allocator<double, 1000> x; // allocator()
/*
1000 bytes
992...992
<after a request for 5 doubles>
-40...-40 944...944
*/

allocate()
deallocate()
construct()  // I've done this for you
destroy()    // I've done this for you
operator[]() // I've done this for you
