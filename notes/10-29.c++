// -----------
// Mon, 29 Oct
// -----------

/*
Please vote!!!

Flawn Academic Center (FAC)
Perry-Casteñada Library (PCL)
M-Sa:  7am-7pm
Su:   12pm-6pm
*/

struct A {
    void f () {
        A* const this;

    void g () const {
        const A* const this;

struct A {};

/*
A()
A(A)
=(A)
~A()
*/

template <typename T>
    class vector {
        private:
            ...
        public:
            vector (initializer_list<value_type> rhs) :
                _b ((rhs.size() == 0) ? nullptr : new value_type[rhs.size()]),
                _e (_b + rhs.size()) {
            copy(rhs.begin(), rhs.end(), _b);}

            vector (const vector& rhs) :
                _b ((rhs.size() == 0) ? nullptr : new value_type[rhs.size()]),
                _e (_b + rhs.size()) {
            copy(rhs.begin(), rhs.end(), _b);}

            vector& operator = (const vector& rhs) {
                delete [] _b;
                _b = (rhs.size() == 0) ? nullptr : new value_type[rhs.size()]);
                _e = _b + rhs.size());
                copy(rhs.begin(), rhs.end(), _b);
                return *this;}

            bool operator == (const vector& rhs) const {

void f (vector<int> y) {
    ...}

int f () {
    vector<int> x(2, 10);
    f(x);

    vector<int> y(3, 20);

    x = y;
    cout << (x == y);
    cout << x.operator==(y);
    return 0;}
