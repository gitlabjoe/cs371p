// -----------
// Fri, 21 Sep
// -----------

T*
const T*
T* const       <-> T&,      array
const T* const <-> const T&

T&
const T&

T a[] = {2, 3, 4};
T(int), 3 times

T a[10] = {2, 3, 4};
T(int), 3 times
T(),    7 times

T a[10];
T(),    10 times

T a[10] = {};
T(),    10 times

equal(a + 10, a + 15, b + 20);

/*
the smallest a can be is 15
the smallest b can be is 25
*/

T x = ...;
T y = x;
