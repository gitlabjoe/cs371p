// -----------
// Mon, 22 Oct
// -----------

/*
Please vote!!!

Flawn Academic Center (FAC)
Perry-Casteñada Library (PCL)
M-Sa:  7am-7pm
Su:   12pm-6pm
*/

const int s = 10;
int a[s];
cout << sizeof(a); // 40

++a; // no

int t;
cin >> t;
int a[t];  // no

int* a = new int[t];
cout << sizeof(a);   // 8

// under the assumption that T is a user type

int* a = new int[n]; // O(1)
delete [] a;         // O(1)

T*   p = new T[n];   // O(n), T(),  n times
delete [] p;         // O(n), ~T(), n times

//++a;


/*
what are the things that I can wrong with delete

1. delete too late
2. not using [] correctly
3. delete the wrong address
4. delete more than once
5. delete too early
*/

int s = 10;
int* a = new int(s);
...
delete a;

/*
memory checker, Valgrind
*/

struct mammal {...}
struct tiger : mammal {...}

int main () {
    tiger*  p = new mammal(...); // no
    mammal* p = new tiger(...);
    return 0;}

int s = ...;
T  v;
T* a = new T[s];   O(n)
fill(a, a + s, v); O(n)

...

delete [] a;       O(n)

{
vector<T> x(s, v); O(n)
}                  O(n)

vector<T> x(y);    O(n), T(T), n times

x = z; // where z is twice a big, ~T(), s times; T(T), 2s times
