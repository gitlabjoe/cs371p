// -----------
// Mon, 10 Sep
// -----------

/*
mcl(10, 100)
b = 10
e = 100
m = 51
mcl(10, 100) = mcl(51, 100)
10 -> 80
15 -> 60
50 -> 100
*/

int i = 2;
f(++i);
f(++2);  // no
++i = 3; // yes in C++; no in C, Java, Python
++++i;

/*
pre-increment takes an l-value and produces an l-value
*/

int i = 2;
f(i++);
f(2++);  // no
i++ = 3; // no
i++++;   // no

/*
post-increment takes an l-value and produces an r-value
*/

/*
memoization
build a cache
array

lazy cache
cache the values when input is read and value computed

eager cache
precompute and cache the values before any input is read
*/

int a[...];
array<int, ...> x;
vector<int> x(...);

/*
meta cache
precompute and cache the values outside of HackerRank
caching cycle lengths doesn't work, too many
cache max cycle lengths, instead
1-1000
1000-2000
2000-3000
...
mcl(500, 2500)
*/

/*
problem author
    1. run out of input
    2. specify the amount of input at the beginning
    3. specify a sentinel at the end
*/

/*
IsPrime
    run it, succeed
    fix the tests
    run it, fail
    fix the code
    run it, succeed
*/
