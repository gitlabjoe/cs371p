// -----------
// Mon, 12 Nov
// -----------

/*
vector
front-loaded array
cost of insertion at the begin:  linear
cost of insertion at the middle: linear
cost of insertion at the end:    amortized const
cost of deletion at the begin:   linear
cost of deletion at the middle:  linear
cost of deletion at the end:     const
cost of indexing:                const
*/

vector<int> x(10, 2);
int*        p = &x[5];
cout << *p;            // 2
x.push_back(3);
cout << *p;            // <undefined>

/*
deque
middle-loaded array of arrays
symmetric
pointers, references, iterators are NOT invalidated when adding the begin or the end
cost of indexing has gone up
cost of insertion at the begin:  amortized const
cost of insertion at the middle: linear
cost of insertion at the end:    amortized const
cost of deletion at the begin:   const
cost of deletion at the middle:  linear
cost of deletion at the end:     const
cost of indexing:                const, it's more expensive than vector's
*/

/*
list
doubly-linked list
cost of insertion at the begin:  const
cost of insertion at the middle: const
cost of insertion at the end:    const
cost of deletion at the begin:   const
cost of deletion at the middle:  const
cost of deletion at the end:     const
cost of indexing:                it would be linear, not available
*/

/*
container adapters:
stack, queue, priority_queue
*/

/*
stack
LIFO
add and remove from the back
that's supported by vector, deque, list
push, pop, top, size, empty
not good to use inheritance, Java does
*/

template <typename T, typename C>
class stack : public C {
/*
    push -> push_back
    pop  -> pop_back
    top  -> back
*/

template <typename T, typename C = deque>
class stack {
    C _my_container;
/*
    push  -> push_back
    pop   -> pop_back
    top   -> back
    size  -> size
    empty -> empty
*/

/*
queue
FIFO
add to back and remove from the front
that's supported by deque, list, NOT by vector
push, pop, front, back, size, empty
*/

template <typename T, typename C = deque>
class queue {
    C _my_container;
/*
    push  -> push_back
    pop   -> pop_front
    front -> front
    back  -> back
    size  -> size
    empty -> empty
*/
