// -----------
// Fri, 30 Nov
// -----------

/*
children must override the parent virtual method:
1. return type
2. name
3. number of args
4. type of args
5. const or not
*/

struct A {
    virtual void f (int) {}};

struct B : A {
    void f (int) {}};

int main () {
    A* p = new B;
    *p.f(2);      // no
    (*p).f(2);
    p->f(2);      // B::f

struct A {
    virtual void f (int) const {}};

struct B : A {
    void f (int) {}};

int main () {
    A* p = new B;
    p->f(2);      // A::f

struct A {...}
struct B : A {...} // poor choice

/*
change only the behavior of B objects
edit class B

change only the behavior of A and B objects
edit class A

change only the behavior of A objects
edit ???
*/

struct A` {...}
struct A : A' {}
struct B : A' {...}

/*
change only the behavior of B objects
edit class B

change only the behavior of A and B objects
edit class A'

change only the behavior of A objects
edit A
*/

/*
abstract class
*/

struct A {
    virtual void f (int) = 0;}; // abstract method

void A::f (int i) { // optional
    ...}

struct B : A {
    void f (int i) {
        A::f(i);

/*
consequences of an abstract method
1. class has become abstract
2. derived classes have to override abstract OR become abstract
3. in Java it's prohibited; in C++ it's optional
*/
'
A x;          // no
A* p = new A; // no
A* q;

struct A {
    ~A () {}};

strucrt B : A {}

int main () {
    A* p = new B; // A() B()
    delete p;     // ~A()

struct A {
    virtual ~A () {}};

strucrt B : A {}

int main () {
    A* p = new B; // A() B()
    delete p;     // ~B() ~A()

struct A {
    virtual ~A () = 0;}; // abstract destructor

strucrt B : A {}

int main () {
    A* p = new B; // A() B()
    delete p;     // ~B() ~A()

/*
consequences of an abstract method
1. class has become abstract
2. derived classes have to override abstract OR become abstract
3. in Java it's prohibited; in C++ it's optional
*/

/*
consequences of an abstract destructor
1. class has become abstract
2. doesn't hold
3. it's still required
*/

// in Java
abstract class A {}

// in C++
struct A {
    virtual ~A() = default;};

struct A {
    virtual void f (int) {}       // this is the method to avoid
    virtual void g (int) final {}
    virtual void h (long) = 0;};


/*
f -> derived can, but don't have to, define f
g -> derived can not define g
h -> derived must define h or become abstract
*/
