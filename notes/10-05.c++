// -----------
// Fri,  5 Oct
// -----------

struct A {
    ...};

struct B : A {
    B (...) :
        A (...) // refinement
        {}

    ~B ()
        {} // refinement

    void f (...) { // replacement, Java's finalize, would too
        ...}

/*
In C++,  constructors and destructors exhibit refinement overriding, everything else is replacement.
In Java, constructors                 exhibit refinement overriding, everything else is replacement.
*/

const T x = ...
const T y = ...
x == y
x.operator==(y)

++x
x.operator++()
++++x

x++
x.operator++(0)
x++++           // no
