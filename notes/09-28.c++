// -----------
// Fri, 28 Sep
// -----------

template <typename T1, typename T2>
bool equal (T1 b, T1 e, T2 x) {
    while (b != e) {
        if (*b != *x)
            return false;
        ++b;
        ++x;}
    return true;}

/*
input         iterator: ++, * (read-only), !=, ==
output        iterator: ++, * (write-only)
forward       iterator: <input iterator>, * (read/write)
bidirectional iterator: <forward iterator>, --
random access iterator: <bidirectional>, <, <, <=, >=, [], + <int>, - <int>, -
*/

/*
iterators are like the glue between data structures and algorithms
*/

equal(begin(x), end(x), begin(y));
// equal needs an input iterator

copy(begin(x), end(x), begin(y));
// copy  needs an input and an output

fill(begin(x), end(x), v);
// fill  needs an forward iterator

reverse(begin(x), end(x));
// reverse needs a bidirectional iterator

sort(begin(x), end(x));
// sort needs a random access iterator

forward_list<int> x;
// implemented with a singly linked list
// forward_list provides a forward_iterator

list<int> x;
// implemented with a doubly linked list
// list provides a bidirecional iterator

vector<int> x;
// implemented with a front-loaded array
// vector provides a random access iterator

// Java
list
    get(index)

list has several children
    arraylist
    linkedlist

sort on ArrayList  is n log n
sort on LinkedList is n**2 log n !!!
