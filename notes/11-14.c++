// -----------
// Wed, 14 Nov
// -----------

/*
.....
..*..
..*..
..*..
.....

.....
.....
.***.
.....
.....
*/

/*
5x5 = 25 cells, 3 alive, 22 dead

cells have neighbors
interior: 8 neighbors
edge:     5 neighbors
corner:   3 neighbors

if alive and 2 or 3 neighbors alive, stay alive
if dead  and 3      neighbors alive, get born
*/

/*
John Conway
*/

/*
60 x 100, 6000 cells, 35 cells alive
evolve 183 times, 490 cells are alive
evolve  40 times, 190 cells are alive
doesn't change even by one

.....
.....
.....
..*..
..*..
..*..
..*..
..*..
.....
..*..
..*..
*/
