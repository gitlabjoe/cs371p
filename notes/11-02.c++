// -----------
// Fri,  2 Nov
// -----------

/*
Please vote!!!

Flawn Academic Center (FAC)
Perry-Casteñada Library (PCL)
M-Sa:  7am-7pm
Su:   12pm-6pm
*/

template <typename T>
class vector {
    private:
        ...
    public:
        vector (initializer_list<value_type> rhs) {
            _b = (rhs.size() == 0) ? nullptr : new value_type[rhs.size()];
            _e = _b + rhs.size();
            copy(rhs.begin(), rhs.end(), _b);}

        vector (const vector& rhs) {
/*
            _b = nullptr;
            *this = rhs;
*/
            _b = (rhs.size() == 0) ? nullptr : new value_type[rhs.size()];
            _e = _b + rhs.size());
            copy(rhs.begin(), rhs.end(), _b);
            }

        vector& operator = (vector rhs) {
            swap(_b, rhs._b);
            swap(_e, rhs._e);
/*
            if ( this == &rhs)
            // if (*this ==  rhs)
                return *this;
            delete [] _b;
            _b = (rhs.size() == 0) ? nullptr : new value_type[rhs.size()]);
            _e = _b + rhs.size());
            copy(rhs.begin(), rhs.end(), _b);
*/
            return *this;}

        ~vector () {
            delete [] _b;}

        bool operator == (const vector& rhs) const {
            return (size() == rhs.size()) && equal(_b, _e, rhs._b);}

int main () {
    vector<int> x(10, 2);
    x = x;
    vector<int> y(20, 3);
    x = y;
    cout << (x == y);
    cout << x.operator==(y);
    return 0;}

int, char, double: builtin
vector, deque, elephant: user

int*, char*, double*: builtin
vector*, deque*, elephant*: builtin

struct A {
 //   friend bool operator == (const A&, const A&) {
   //     ...}

    A (int) {}

    friend bool operator == (const A&) const {
        ...}};


void f (A y) {}

int main () {
    A x(2);
    f(x);
    f(A(3));
    f(4);

    A y(5);
    cout << (x == y);
    //cout << x.operator==(y);
    cout << operator==(x, y);

    cout << (x == 5);
    //cout << x.operator==(5);
    cout << operator==(x, 5);

    cout << (5 == x);
//    cout << 5.
    cout << operator==(5, x);

    return 0;}

/*
friend
class make the declarations of friendship
the target of the declaration could be another class, method, function
what access a friend? anything, everything

friendship is not symmetric
friendship is not transitive
*/
