// -----------
// Wed, 26 Nov
// -----------

struct A {
    void f (int) {}};

struct B : A {
    void f (int) {}};

int main () {
    B x;
    x.f(2);    // B.f
    return 0;}

struct A {
    void f (int) {}};

struct B : A {
    void f (int i) {
        return A::f(i);
    void f (string) {}};

int main () {
    B x;
    x.f("abc");
    x.f(2);    // doesn't compile
    x.A::f(2);
    return 0;}

struct A {
    void f (int) {}};

struct B : A {
    using A::f;
    void f (string) {}};

int main () {
    B x;
    x.f("abc");
    x.f(2);
