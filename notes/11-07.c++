// -----------
// Wed,  7 Nov
// -----------

template <typename T, typename A = allocator<T>>
class my_vector {
    private:
        A       _a;
        pointer _b = nullptr;
        pointer _e = nullptr;

    public:
        explicit my_vector (size_type s = 0, const_reference v = value_type(), const A& a = A()) :
                _a (a) {
            _b = (s == 0) ? nullptr : _a.allocate(_s);
            _e = _b + s;
            uninitialized_fill(_a, begin(), end(), v);}

        ~my_vector () {
            _destroy(_b, _e);
            _a.deallocate(_b, size());}

int main () {
    vector<int>                       x(10, 2);
    vector<double>                    y(20, 3);
    allocator                         a(...);
    vector<double, allocator<double>> z(30, 4, a);
    return 0;}
