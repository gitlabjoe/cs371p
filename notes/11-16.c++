// -----------
// Fri, 16 Nov
// -----------

/*
priority_queue
highest priority out
binary heap, log push, log pop
that's supported by deque, vector, NOT by list
add to back and remove from the front
elements must be comparable or a comparator must be provided
*/

template <typename T, typename C = vector, typename BP = less<T>>
class priority_queue {
    C _my_container;
/*
    push  -> heap_push
    pop   -> heap_pop
    top   -> top, only one, read only
    size  -> size
    empty -> empty
*/

/*
set           -> TreeSet (Java), ordered
keys must be comparable or a comparator must be provided
unordered_set -> HashSet (Java)
keys must be hashable
*/

/*
map           -> TreeMap
keys must be comparable or a comparator must be provided
unordered_map -> HashMap
keys must be hashable
*/

class shape {
    private:
        int _x;
        int _y;
    ...

class circle : public shape {
    private:
        int _r;
    public
        circle (int x, int y, int r) :
            shape (x, y)
            _r (r)
            {}

/*
replacement overriding: finalizers, all other methods
refinement  overriding: constructors (top down), destructors (bottom up)
*/

shape* p;
if (...)
    p = new circle(...);
else
    p = new triangle(...);
cout << *p.area();         // no
cout << (*p).area();       // 0
cout << p->area();         // 0
