// -----------
// Wed,  5 Dec
// -----------

vector<Circle> x(s, Circle(...));
C(...), once
C(C),   s times
~C(),   once

vector<AbstractShape> x(s, Circle(...)); // no

vector<AbstractShape*> x(s, Circle(...)); // no

vector<AbstractShape*> x(s, new Circle(...)); // compiles, but not what we want
C(...), once

class AbstractShape ...
class Circle ...
class Triangle ...

{
Shape x = new Circle(...);
Shape y = new Triangle(...);

Shape z = y;
z = x;
}

class Shape {
    private:
        AbstractShape* _p;
    public:
        Shape (AbstractShape* p) {
            _p = p;}

        ~Shape () {
            delete _p;}};

        Shape (const Shape& rhs) {
            _p = rhs._p->clone();}|

        Shape& operator = (const Shape& rhs) {
            delete _p;
            _p = rhs._p->clone();
            return *this;}
