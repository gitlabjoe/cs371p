// -----------
// Fri,  7 Sep
// -----------

k = i + j;
k = 2 + 3;
(2 + 3) = k; // no

i += j;
i += 2;
3 += 2;       // no
(i += 2) = 3;

/*
(3n + 1) / 2
3n/2 + 1/2
n + n/2 + 1/2
n + n/2 + 1
n + (n >> 1) + 1
*/

/*
unit testing is called white box testing
you can see the internals of the solution and test them

acceptance testing is called black box testing
you can't see the internals
you can only see the external behavior of the program
/*

/*
write code that is easy to run and easy to test

Collatz.h/c++
	kernel
	the functions that really solve the problem
	oblivious about where input is coming from and where output is going to

RunCollatz.c++
	#include "Collatz.h"
	run harness

TestCollatz.c++
	#include "Collatz.h"
	test harness
	Google Tests
/*

/*
clone the code repo
run RunCollatz,  all tests succeed
run TestCollatz, all tests succeed
fix the tests
run the tests, all tests fail
fix the code
run the tests, all tests succeed
HackerRank
generate more tests
optimize the code
run the tests, all tests succeed
HackerRank
repeat...
/*

/*
REPL
read eval print
*/
