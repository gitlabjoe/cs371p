// -----------
// Fri, 26 Oct
// -----------

/*
Please vote!!!

Flawn Academic Center (FAC)
Perry-Casteñada Library (PCL)
M-Sa:  7am-7pm
Su:   12pm-6pm
*/

template <typename T>
class vector {
    private:
        T* _b;
        T* _e;

    public:
        vector () {
            _b = nullptr;
            _e = nullptr;}

        vector (size_t s) {
            _b = ((_s == 0) ? nullptr : new T[s]);
            _e = _b + s;
            fill(_b, _e, T());}

        vector (size_t s, const T& v) {
            _b = ((_s == 0) ? nullptr : new T[s]);
            _e = _b + s;
            fill(_b, _e, v);}

        vector (initializer_list<T> l) {
            _b = ((l.size() == 0) ? nullptr : new T[s]);
            _e = _b + s;
            copy(std::begin(l), l.end(), _b);}

        size_t size () const {
            return _e - _b;}

        const T& operator [] (size_t i) const {
            return _b + i;}

         T& operator [] (size_t i)  {
            return _b + i;}
