// -----------
// Wed, 26 Sep
// -----------

bool equal (const int* b, const int* e, const int* x) {
    while (b != e) {
        if (*b != *x)
            return false;
        ++b;
        ++x;}
    return true;}

bool equal (const int* b, const int* e, const long* x) {
    while (b != e) {
        if (*b != *x)
            return false;
        ++b;
        ++x;}
    return true;}

template <typename T1, typename T2>
bool equal (T1 b, T1 e, T2 x) {
    ? = e - b;
    while (b < e) {
        if (*b != *x)
            return false;
        ++b;
        ++x;}
    return true;}

// 1st call
T1 = int*
T2 = int*

// 2nd call
T1 = int*
T2 = long*

// 3rd call
T1 = list<int>::iterator
T2 = list<int>::iterator

// 4th call
T1 = list<int>::iterator
T2 = vector<int>::iterator
