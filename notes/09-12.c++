// -----------
// Wed, 12 Sep
// -----------

/*
what should we use for user errors
not assertions
exceptions
*/

/*
let's pretend we don't have exceptions
*/

// use the return

int f (...) {
    ...
    if (<something wrong>)
        return <special value>;
    ...}

int g (...) {
    ...
    int i = f(...);
    if (i == <special value>)
        <something wrong>l
    ...}

// use a global

// foo.c++

int h = 0;                   // global definition

int f (...) {
    ...
    if (<something wrong>)
        h = <special value>;
        return ...;
    ...}

// bar.c++

extern int h;                // global declaration

int g (...) {
    ...
    h = 0;
    int i = f(...);
    if (h = <special value>)
        <something wrong>;
    ...}

// use a parameter, int doesn't work

int f (..., int e2) {
    ...
    if (<something wrong>)
        e2 = <special value>
        return ...
    ...}

int g (...) {
    ...
    int e1 = 0;
    int i  = f(..., e1);
    if (e1 == <special value>)
        <something wrong>
    ...}

// Java
// use a parameter, int doesn't work

int f (..., int e2) {
    ...
    if (<something wrong>)
        e2 = <special value>;
        return ...;
    ...}

int g (...) {
    ...
    int e1 = 0;
    int i  = f(..., e1);
    if (e1 == <special value>)
        <something wrong>;
    ...}

// Java
// use a parameter, Integer doesn't work

int f (..., Integer e2) {
    ...
    if (<something wrong>)
        e2 = <special value>;
        return ...;
    ...}

int g (...) {
    ...
    Integer e1 = 0;            // boxing
    int i  = f(..., e1);
    if (e1 == <special value>) // unboxing
        <something wrong>;
    ...}

// C++

/*
two tokens:   *, &
two contexts: on a type, on a variable
*/

int main () {
    int i = 2;
    int j = i;   // copy value
    ++j;
    cout << i;   // 2

    int  i = 2;
    int* p = i;  // no
    int* p;      // pointer
    p = &i;      // copy address
    ++p;         // compiles, but isn't what we want
    ++*p;
    cout << i;   // 3
    int j = 4;
    p = &j;      // pointer can point elsewhere, later
    ++*p;
    cout << j;   // 5

    int  i = 2;
    int& r = &i; // no
    int& r;      // no
    int& r = i;  // reference, refers to i, permanently
    int  j = 4;
    r = j;
