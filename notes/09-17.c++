// -----------
// Mon, 17 Sep
// -----------

int& pre_incr (int& i) {
    i += 1;
    return i;}

int post_incr (int& i) {
    int j = i;
    i += 1;
    return j;}

void f (int v) {
    ++v;}

int main () {
    int i = 2;
    f(i);
    cout << i; // 2

void g (int* p) {
    ++p;}

int main () {
    int i = 2;
    // g(i);   // no
    g(&i);
    cout << i; // 2

void g (int* p) {
    ++*p;         // undefined if p is 0
    int j = 3;
    p = &j;}

int main () {
    int i = 2;
    // g(i);   // no
    g(&i);
    cout << i; // 3
    g(185);    // no
    g(0);      // yes!!!

void h (int& r) {
    // ++*r;
    ++r;}

int main () {
    int i = 2;
    // h(&i);  // no
    h(i);
    cout << i; // 3
    int j = 3;
    h(j);
    h(185);    // no
    h(0);      // no
