// -----------
// Mon,  5 Nov
// -----------

vector<int> f (...) {
    vector<int> x(10, 2);
    ...
    return x;}

int main () {
    vector<int> y = f(...);
    return 0;}

/*
move() simply casts an l-value into an r-value
*/

/*
== depends on equal()
<  depends on lexicographical_compare()
*/

template <typename T>
class vector {
    private:
        ...

    public:
        my_vector ( my_vector&& rhs) {
            swap(that);}

        my_vector& operator = ( my_vector&& rhs) {
            if (this == &rhs)
                return *this;
            my_vector that(move(rhs));
            swap(that);
            return *this;}
