// -----------
// Mon,  1 Oct
// -----------

/*
accumulate
s @ a1 @ a2 @ a3 @ ... @ an-1

s = 0
@ = +
sum

s = 1
@ = *
product

s = 1
@ = *
values are 1 to N
factorial
*/

// Java
class A {
    public void f () ...    // anyone with an object
    protected void g () ... // any descendent or same package with an object
    private void h () ...   // only A
    void m () ...           // package only with an object

// C++
(class or struct) A {
    public: void f () ...    // anyone with an object
    protected: void g () ... // any descendent with an object
    private: void h () ...   // only A
    void m () ...            // class: private; struct: public; with an object

/*
convention
if everything is public, use struct
if not, use class
*/

template <typename T>
class RangeIterator {
    private:
        T _v;

    public:
        RangeIterator (const T& v) {
            _v = v;}
