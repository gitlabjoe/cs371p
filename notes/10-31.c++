// -----------
// Wed, 31 Oct
// -----------

/*
Please vote!!!

Flawn Academic Center (FAC)
Perry-Casteñada Library (PCL)
M-Sa:  7am-7pm
Su:   12pm-6pm
*/

class instruction {
    ...

class species {
    vector<instruction> _intrs;

enum direction {...};

class creature {
    private:
        species*  _s;
        int       _pc;
        direction _dir;
    public:
        ? take_a_turn (?)
        ? what_direction (?)
        ? change_species (?)

class darwin {
    public:
        vector<vector<creature*>> _grid;

    public:
        darwin

int main () {
    vector<int>         x(10, 2);
    vector<vector<int>> y(20, x);
    vector<vector<int>> z(20, vector<int>(10, 2));
    darwin d(10, 20);
    return 0;}
