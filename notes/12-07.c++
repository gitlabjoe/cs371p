// -----------
// Fri,  7 Dec
// -----------

class Shape {
    private:
        AbstractShape* _p;

    public:
        ...

        AbstractShape& operator * () {
            return *_p;}

        AbstactShape* operator -> () {
            return _p;}

Shape p = new Circle(...);
cout << *p.area();         // no
cout << (*p).area();
cout << p.operator*().area();

cout << p->area();
cout << p.operator->()->area();

vector<Shape> x(s, new Circle(...));
S(...), once
C(...), once
S(S),   s times
C(C),   s times
~S(),   once
~C(),   once

/*
copy on write
*/

Shape x = new Circle(...);
Shape y = x;
Shape z = x;
cout << x.area();
z.move(...);
z.move(...);

shared_ptr
unique_ptr
