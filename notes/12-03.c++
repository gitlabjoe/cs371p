// -----------
// Mon,  3 Dec
// -----------

class AbstractShape {
    public:
        virtual AbstractShape* clone () const = 0;
        virtual double         area  () const = 0;

class Triangle : public Shape {
    public:
        Triange (int x, int y, int w, int h) :
            Shape(...),
            ...
            {}
            }

        Triangle* clone () const {       // return type are covariant
            return new Triangle(*this);}

        double area () const {
            return .5 * w * h;}

int main () {
    Triangle* p = new Triangle(...);
    Triangle* q = p->clone();

    AbstractShape* p;
    if (...)
        p = new Circle(...);
    else
        p = new Triangle(...);
    cout << p->area();

/*
inlining a function replaces the call with the code
benefit is performance
*/

Circle x(...);
cout << x.area(); // 1. on an object

AbstractShape* p = new Circle(...);
p->AbstractShape::read(...);        // 2. with the scope operator
*/

struct A {
    A () {
    	f();}            // 3. in constructor

    virtual ~A () {
        f();}            // 4. in destructor

    virtual void f () {}
    };

struct B : A {
     void f () {}
    };

struct C : B {
     void f () {}
    };

int main () {
    A* p = new C;
    p->f();       // C.f
    delete p;
