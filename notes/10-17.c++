// -----------
// Wed, 17 Oct
// -----------

#include <memory> // allocator

int n;
cin >> n;

T* a = new T[n];   // O(1) if T is built-in type; O(n) if it's a user type
                   // T(), n times
T v(...);
fill(a, a + n, v); // =(T), n times

// <work with a>

delete    a; // undefined
delete [] a; // O(1) if T is built-in type; O(n) if it's a user type
             // ~T(), n times

{
vector<T> x(n, v); // T(T), n times
// <work with x>
}

allocator<T> x;

T* a = x.allocate(n);        // <nothing>, pure allocation, like malloc
for (int i = 0; i != n; ++i) // O(n)
    x.construct(a + i , v);  // T(T)

delete [] a; // no, undefined

for (int i = 0; i != n; ++i) // O(n)
    x.destroy(a + i);        // ~T()

x.deallocate(a); // O(1)

allocator<double> x;       // 8 bytes in a double
double* a = x.allocate(5); // we need 40 bytes, O(n)
loop
    construct

// <work with a>

loop             // O(n)
    destroy
x.deallocate(a); // O(1)
