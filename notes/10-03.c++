// -----------
// Wed,  3 Oct
// -----------

1 - 2 - 3 - 4

template <typename II, typename T, typename BF>
T accumulate (II b, II e, T v, BF f)

#include <utility> // !=, >, <=, >=

using namespace std::rel_ops;

template <typename T>
class RangeIterator {
    private:
        T _v;

    public:
//        RangeIterator (const T& v) { // T()  on _v
//            _v = v;}                 // =(T) on _v

        RangeIterator (const T& v)
            : _v (v)                   // member initialization list, T(T)
            {}

        T operator * () {
            return _v;}

        bool operator == (const RangeIterator& rhs) const {
            return _v == rhs._v;}

//      you'll get it for free
//        bool operator != (RangeIterator rhs) {
//            return !(*this == rhs);}

        RangeIterator& operator ++ () {
            ++_v;
            return *this;

        const RangeIterator operator ++ (int) {
            RangeIterator tmp(*this);
            ++_v;
            return tmp;}

int i = 2;
int j = i;
int k;
k = i;

T x = ...; // T(...)
T y = x;   // T(T), copy constructor
T z;       // T()
z = x;     // =(T)
