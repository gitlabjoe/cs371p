// -----------
// Wed,  5 Sep
// -----------

/*
operator overloading
1. you can NOT define an operator (i.e. you can't invent a new token)
2. you can NOT overload an operator on a built-in type, even if it doesn't already have a definition
3. you can overload an operator on a user type

4. you can NOT change the precedence
5. you can NOT change the arity
6. you can NOT change the associativity

7. you can change the l-value/r-value nature of the args and the return of an operator
*/

x * y + z // * has a higher precedence than +

x * y // * is binary,    the arity of an operator
*p    // * is also unary

x * y * z // * is left associative
x = y = z // = is right associative

int i = 2;
int j = 3;
int k = (i + j);
i + j;           // can't standalone, an expression

i += j;          // can standalone, a statement

k = (i += j);    // C, Java, C++, not in Python

(i += j) = k;    // C++, not C, not Java, not Python

l-values = r-values

++i;
f(++i); // f gets the incremented i

i++;
f(i++); // f gets the unincremented i

// post-increment is slower than pre-increment, must make a copy

for (I i = 0; i != s; ++i) { // always use pre-increment
    ...}

int i = 2;
int j = 3;
int k = (i << j);

// << on int is a pure r-value operator

cout << flush;
// << on ostream takes an l-value on the left, an r-value on the right
// and returns an l-value

(cout << flush) << endl;

/*
assertions
unit testing
coverage
*/

/*
take pos int
if even, divide   by 2
if odd,  multiply by 3 and add 1
repeat until 1
*/

1
5 16 8 4 2 1

/*
cycle length of  1 is 1
cycle length of  5 is 6
cycle length of 10 is 7
*/

/*
assertions are good for preconditions, post conditions, and loop invariants

assertions are not good for testing, Google Test is
assertions are not good for user error
*/
