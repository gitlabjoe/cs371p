// -----------
// Fri, 14 Sep
// -----------

int f (...) {
    ...
    if (<something wrong>)
        throw Mammal(...);
    if (<something wrong>)
        throw Tiger(...);
    ...

int g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
//    catch (Tiger x) { // double copy
//        ...}
    catch (Mammal x) { // double copy and a slice
        ...}
    ...

...

/*
in Java builtin types are on the stack
user types are on the healp

in C++ either can be on either

in C++ throw always copies its argument
*/

int f (...) {
    ...
    if (<something wrong>)
        throw new Mammal(...);
    if (<something wrong>) {
        Tiger atiger(...);   // no good, object will die
        throw &atiger;}
    ...

int g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (Tiger* x) {
        ...}
    catch (Mammal* x) {
        ...}
    ...

/*
don't catch by value
don't catch by address
always catch by reference
*/

int f (...) {
    ...
    if (<something wrong>)
        throw Mammal(...);
    if (<something wrong>) {
        throw Tiger(...);}
    ...

int g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (Tiger& x) {
        ...}
    catch (Mammal& x) {
        ...}
    ...

string s = "abc";
string t = "abc";
cout << (s  == t);  # true,  s.equals(t)
cout << (&s == &t); # false, s == t

cout << (s == "abc");

void f (string s) {
    }

int main () {
    string s = "abc";
    f(s);
    f("abc");         // implicit type conversion from char* to string
