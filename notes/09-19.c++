// -----------
// Wed, 19 Sep
// -----------

// convert logic errors and runtime errors into compile time errors

void f (int* p) {
    ++p;          // easy to introduce a subtle logic error
    ++*p;}

int main () {
    int i = 2;
    f(&i);
    f(185);    // not an address
    f(0);      // runtime error
    cout << i; // 3

void g (int& r) {
    ++*r;         // can't introduce a subtle logic error, doesn't compile
    ++r;}

int main () {
    int i = 2;
    f(i);
    f(185);    // not an l-value
    f(0);      // not an l-value
    cout << i; // 3

// const

int i; // initialization is optional
i = 2;
++i;

const int ci;     // initialization is required
const int ci = 3;
++ci;             // no

int* p;
p = &i;
++*p;

p = &ci; // no

// pointer to constant
// read-only pointer
// the int is immutable, the pointer is mutable

const int* pc; // initialization is optional
pc = &ci;
++*pc;         // no
++pc;

pc = &i;
++*pc;         // no

// constant pointer
// read/write pointer
// the int is mutable, the pointer is immutable

int* const cp;       // initialization is required
int* const cp = &ci; // no
int* const cp = &i;
++*cp;
++cp;                // no

int* const p = new int[1000];
...
++*p;
...
++p;  // can't introduce a subtle logic error, doesn't compile
++*p;
delete [] p;

// constant pointer to constant
// read-only pointer
// the int is immutable, the pointer is immutable

const int* const cpc = &ci;
const int* const cqc = &i;
