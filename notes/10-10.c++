// -----------
// Wed, 10 Oct
// -----------

/*
ACM competitive programming workshop
Wed, 10 Oct, 6pm, GDC 4.302
*/

T x;
x = y;

T x = y;

struct A {
    A () {}

    ~A () {}

    A (A)           // no
    A (A&)          // no
    A (const A&) {}

    A& operator = (const A&) {
        return *this;}};

void f (A z) {
    ...
    }

int main () {
    A x;
    A y = x;
    f(x);
    y = x;     // y.operator=(x);

    int i = 2;
    int j = 3;
    int k = (i = j);
    (i = j) = k;
    (i = j)++;
    }

struct A () {
    A (int) {}};

void f (A z)
    {}

int main () {
    A x;     // no
    A x = 2; // A(int)
    f(x);    // A(A)

    f(A(3)); // A(int) A(A) -> A(int)

    f(4);    // A(int) A(A) -> A(int)

void f (string s) {
    ...}

int main () {
    string t = "abc";
    f(t);
    f("abc")          // ok

void f (vector<int> y) {
    ...}

int main () {
    vector<int> x(100);
    f(x);
    f(100);             // no
