// -----------
// Mon, 19 Nov
// -----------

Shape x(...);
Shape y(...)_;
cout << x << y;
cout.operator<<(x);  // operator << a method, no
operator<<(cout, x); // operator << a function

Shape y(...);
cout << (x == y);
cout << x.operator==(y);  // operator == is a method
cout << operator==(x, y); // operator == is a function

String x("abc")
String y("abc")
cout << (x == y);            // true
cout << (x == "abc");        // true
cout << operator==(x, "abc");
cout << x.operator==("abc"); // operator == is a method
cout << ("abc" == x);        // no
cout << "abc".
cout << operator==("abc", x);

class Shape {
    friend ostream& operator << (ostream& lhs, const Shape& rhs) {
        lhs << rhs._x;
        lhs << rhs._y;
        return lhs;}

    private:
        int _x;
        int _y;
    ...};
