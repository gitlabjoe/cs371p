// -----------
// Mon,  8 Oct
// -----------

/*
s @ a1 @ a2 @ a3 @ ... @ an-1

s = 0
@ = +
sum

s = 1
@ = *
product

s = 1
@ = *
a = [1..n]
factorial
*/

template <typename II, typename T, typename BF>
T accumulate (II b, II e, T v, BF f) {
    while (b != e) {
        v = f(v, *b)
        ++b;}
    return v;}

for (T v : a) {
    ...

auto b = begin(a);
auto e = end(a);
while (b != e) {
    v = *b;
    ...
    ++b;}

class Range {
    class iterator {
        ...
