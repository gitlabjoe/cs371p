// -----------
// Fri, 31 Aug
// -----------

/*
Docker
    same dev env on N machines
    same dev env for M users
*/

/*
continuous integration
*/

/*
there's NO Java analog to C++'s #include
using in C++ is the analog to import in Java
/*

/*
the entry point of a program
C++ only has one entry point
Java has many entry points
*/

int i = 2;
int j = 3;
int k = (i << j);

/*
<< is a binary operator, takes two ints and produces an int

operator overloading
1. you can NOT define an operator (i.e. you can't invent a new token)
2. you can NOT overload an operator on a built-in type, even if it doesn't already have a definition
3. you can overload an operator on a user type
*/

/*
type of cout: ostream
ostream overloaded << multiple times for all the built-in types
*/

int i = 2;
cout << i;

float f = 3.4;
cout << f;

cout << "\n"; // new line

/*
output to be buffered
*/

cout << flush; // flush the buffer
cout << endl;  // a combination of "\n" and flush
