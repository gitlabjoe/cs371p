// -----------
// Wed, 24 Oct
// -----------

/*
Please vote!!!

Flawn Academic Center (FAC)
Perry-Casteñada Library (PCL)
M-Sa:  7am-7pm
Su:   12pm-6pm
*/

template <typename T>
class vector {
    private:
    /*
        T*  _a;
        int _s;
    */
        T* _b;
        T* _e;
    public:
        vector ()    {...}
        vector (int) {...}
        vector (int, T) {...}
        vector (initializer_list) {...}

        int size () const {return 0;};

        T* begin () const {...}

        const T& operator [] (int) const {...}
        T& operator [] (int) {...}

int main () {
    vector<int> x(...);
    ...begin(x)...
    ...x.begin()...
    cout << x[1];
    cout << x.operator[](1);
    return 0;}

struct A {};

/*
A()
A(A)
~A()
=(A)
*/
