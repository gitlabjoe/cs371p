// -----------
// Mon, 26 Nov
// -----------

Shape x(2, 3);
cout << x;
cin  >> x;

class Shape {
    friend bool operator == (const Shape& lhs, const Shape& rhs) {
        return lhs.equals(rhs);
    private:
        ...
    protected:
        virtual bool equals (const Shape& rhs) {
            return (x == rhs.x) && (y == rhs.y);}
    public:
        virtual ~Shape () = default;
        virtual double area () const {
            return 0;}

class Circle : public Shape {
    private:
        ...
        bool equals (const Shape& rhs) // has to have same signature
                                       // return type, name, number of args, type of args, whether it's const
            return Shape::equals(rhs) && (r == rhs.r)

Shape* p;
if (...)
    p = new Circle(...);
else
    p = new Triangle(...);
cout << p->area();

/*
In Java, dynamic binding is the default
In C++,  static  binding is the default, and virtual turns on dynamic binding
*/

/*
compile-time casts
    static_cast
    const_cast
    reinterpret_cast

run-time cast
    dynamic_cast
*/
